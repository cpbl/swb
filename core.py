#!/usr/bin/python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
__author__="cpbl"
"""
plotting / analysis tools for 11-point  subjective response data, etc.

"""
from cpblUtilities.matplotlib_utils import toprighttext, toplefttext,bottomrighttext

def plot_stacked_bar_by_SWL_for_partitioned_sample(df,
        ax=None,
                                                   colors=None,
        **args,
):
    """
    This was created for plotting the histogram of latent SWB for three classes of low type (ie 0, 5, 10). So we want stacked bars of the three types.
Pass a dataframe with columns: 'SWL' (latent value),'prob' (height of bar segment) and index 'focal_value' (subclass).

    The trick done here is that we want the legend to order items from top to bottom, opposite to order of plotting.
     A considreable problem is that not all SWL values may be obsserved in each group.


    """
    # First, rectangularize DataFrame before cumsum
    from cpblUtilities.pandas_utils import rectangularize_dataframe
    iname = df.index.name
    assert iname
    dfr= rectangularize_dataframe(df.reset_index(), [iname, 'SWL',]).fillna(0).set_index(iname)
    # Fix the index order!  The line above messes it up for some reason.
    dfr = dfr.loc[list(df.index.unique())]
    
    bottoms = dfr.groupby('SWL').prob.cumsum()

    indices = df.index.unique()[::-1]
    if colors is None: colors = [None]*len(indices)
    
    for ifv, fv in  enumerate(indices):
        adf = df.loc[fv]
        bottom= 0 if ifv== len(indices)-1 else bottoms.loc[indices[ifv+1]]
        plot_bar_by_SWL(adf.SWL.values, adf.prob.values,
                        bottom =  bottom,
                        color = colors[::-1][ifv],
                        label = fv,
                        ax=ax,
                        **args
                        )
    
    

# This is the simplest SWL hist tool, though I've been using the overkill plot_predicted_distribution (below) recently (oops)
def plot_bar_by_SWL(swl,y=None,fv=None, ax=None, bottom=None,  color=None, zorder =None, ec='k',
                    step=False, # Ignore bottom, width, color and only show the envelope of the full-width bars.
                    linestyle=None, alpha=None, xlabel='SWL response', label=None, weight=None, linewidth=None):

    """

    N.B. histogram_with_full_support(y, all_swb_values, colname=None, normalize=False) may be useful for calling form B(?) 

    Calling form A:
    plot_bar_by_SWL([0,1,2,3,4,5,6,7,8,9,10], [22,34,45,23,12,34,45,56,34,23,12,45], fv=[0,5,10], ax=None, bottom=None,  color=None, zorder =None, ec='k', alpha=None)
         swb is a list like 
         y is something like [sum(swl==ii) for ii in self.allSWLvalues], ie the counts corresponding to each value in swb
         This form accomodates weighted data.


    Calling form B:
    plot_bar_by_SWL(swb_list, 
                     fv=None, ax=None, bottom=None,  color=None, zorder =None, ec='k', alpha=None)
         swb is a list (or pd.Series)  of all observations of swb

    
    Less simple: to plot a shaded range, pass the top-bottom as y, and the bottom as bottom 


    Calling form C:  !? not done?
    plot_bar_by_SWL(swb_observations, weight=weight_var,)
         swb is a list (or pd.Series)  of all observations of swb


    Calling form D:
    plot_bar_by_SWL(pd.Series([22,34,45,23,12,34,45,56,34,23,12,45], index=[0,1,2,3,4,5,6,7,8,9,10]), fv=[0,5,10], ax=None, bottom=None,  color=None, zorder =None, ec='k', alpha=None)
         swb is a Series giving both SWL values and counts or proportions.
         This form accomodates weighted data.

    """

    if fv is None: fv=[]
    if ax is None: ax=plt.gca()
    width=1

    # Calling form B and C and D
    if y is None:
        # Calling form D
        if isinstance(swl, pd.Series) and len(swl) in [11,10]: # This is a lookup from SWL value to proportion or counts
            swl, y = swl.index, swl.values
        # Calling form B or C
        elif (isinstance(swl, pd.Series) or isinstance(swl, list) or isinstance(swl, np.array))          and len(swl)>11: # This is a lis tof observations
            swl=swl.dropna().astype(int)
            assert len(swl.unique())<12
            if weight is None:
                swl=swl.dropna()
                assert len(swl.unique())<12
                hist_ =swl.value_counts().sort_index()
                swl,y = hist_.index, hist_.values
            else:
                # For now, weight should be a vector, not variable  quick fix
                #  (dfx.isFive*dfx.weight).sum()/dfx.weight.sum()
                # Guess all values:
                if fv not in [None, []]:
                    oud= common_swb_scales(fv[-1]-fv[0] +1)
                    bins,allswl = oud['edges'],oud['centers']
                else:
                    allswl = range(np.min(swl), np.max(swl)+1)
                    bins= list(    np.array( allswl) -0.5 )
                    bins = bins +[bins[-1]+1]
                print(' Doing a weighted histogram...')
                hist_,edges_ = np.histogram(swl, bins= bins, range=None, normed=None, weights=weight)
                swl,y = allswl,  hist_/np.sum(hist_)


    if isinstance(swl,pd.Series):
        swl=swl.values
    if isinstance(y,pd.Series):
        y=y.values
    if step:
        assert width==1 and bottom is None
        hh=ax.step(swl+.5, y,                                               color = ec, zorder=zorder, alpha=alpha, label=label, linestyle=linestyle, linewidth =linewidth)
    else:
        hh=ax.bar(swl, y, width=width, align='center', bottom=bottom, color = color,  ec=ec, zorder=zorder, alpha=alpha, label=label, linestyle=linestyle, linewidth =linewidth)
    xticks_with_colored_focal_values(swl, fv, ax)
    """
    ax.set_xlim([ swl[0]-0.5, swl[-1]+0.5])
    ax.set_xticks(list(swl))
    ax.set_xticklabels([str(s) for s in swl])
    [t.set_color('r' if swl[ii] in fv else 'k') for ii,t in enumerate(ax.xaxis.get_ticklabels())]
    """
    ax.set_xlabel(xlabel)
    return hh


def xticks_with_colored_focal_values(swl, fv, ax, xlabel=None):#'SWL response'):
    if isinstance(swl,int):
        sdict=common_swb_scales(swl)
        foooo
    if fv is None:
        fv=common_swb_scales(len(swl))['fv']

    ax.set_xlim([ swl[0]-0.5, swl[-1]+0.5])
    ax.set_xticks(list(swl))
    ax.set_xticklabels([str(s) for s in swl])
    [t.set_color('r' if swl[ii] in fv else 'k') for ii,t in enumerate(ax.xaxis.get_ticklabels())]
    if xlabel is not None:
        ax.set_xlabel(xlabel)

    
    

#See plot_predicted_distribution in mixture_models.py.  Here I'm trying to externalize some of that.
# This is for when I have probability of high type, and probabilities for each response already calculated.
# Should be renamed plot_probability_distribution ?
def plot_predicted_distribution(Plows, Phighs, ax=None, allSWLvalues=None, textbox_title=None, labels=None, frameon=True, **args):
    """
If you have the following: 
     'pHigh', 'pH0', 'pH1', 'pH2', 'pH3', 'pH4', 'pH5', 'pH6', 'pH7', 'pH8', 'pH9', 'pH10', 'pL0', 'pL5',  'pL10'
then Plows (a three-tuple)  is [pL0, pL5, pL10] * (1-pHigh); and analogously for Phighs (an 11-tuple)

You can pass edge color, etc, as **args for the high-type ax.bar
"""
    """ 
huh??: FALSE: This was a misunderstanding. The problem is just that Im plotting ologit. In that case, make sure the alphaL are small. ((("Until the major bugfixes in 201907, the alpha values were scaled like SWL values, whereas now they are centered around zero.  Until I understand this well, I'm making two modes. SCALING2019=True means show the top axis from about -4 to +4, not 0 to 10.")))
        SCALING2019=True

        """
    if allSWLvalues is None: allSWLvalues = range(11)#[0,1,2,3,4,5,6,7,8,9,10]
    if labels is None:
        labels={'low':'low type','high':'high type'}
    focalvalues=[0,5,10] if len(allSWLvalues)==11 else [1,5,10] if len(allSWLvalues)==10 else [1,4,7] if allSWLvalues==[1,2,3,4,5,6,7] else None
    assert focalvalues
    if len(Plows) == len(focalvalues):
        Plows = np.array([ Plows[0], 0,0,0,0, Plows[1], 0,0,0,0, Plows[2]])
    bins = np.append(np.array(range(11))-.5,[10.5])
    #ax.hist(self.ydata, bins=bins, histtype='step', color='m', label='Data')
    #cpblUtilities.mathgraph.transbg(plt.legend(loc='upper left'))
    defargs = dict(color='b',width=1, alpha=.3)
    defargs.update(**args)
    hb= ax.bar(allSWLvalues, Phighs, label=labels['high'], **defargs)
    if len(Plows):
        hb2=ax.bar(allSWLvalues, Plows, bottom = Phighs, color='r',width=1, alpha=.3, label=labels['low'])
    else:
        hb2=None
    if 0: ax.set_ylabel('mean $P$')
    sH,sL = sum(Phighs), sum(Plows)
    assert abs(sH+sL -1 ) <1e-6 # Assert is too strong; should raise warning here

    # Set all xticks
    ax.set_xticks(allSWLvalues)
    [t.set_color('r' if allSWLvalues[ii] in focalvalues else 'k') for ii,t in enumerate(ax.xaxis.get_ticklabels())]
    plt.setp([ax], 'xlim',[allSWLvalues[0]-.5, allSWLvalues[-1]+.5])
    #plt.setp([ax], 'xticks',allSWLvalues)
    #plt.setp([ax,ax2,ax3], 'xticklabels',[])
    if 0: plt.setp([ax], 'xticklabels',[str(xtl) for xtl in allSWLvalues])


    if len(Plows): 
        meanH = (Phighs*np.arange(11)).sum()/sum(Phighs)
        meanL   =  (Plows*np.arange(11)).sum()/sum(Plows)
        meanT  =    ((Plows+Phighs)*np.arange(11)).sum()
        fracL =   sL/(sH+sL)
        #print(Phighs, Plows)
        #print( meanH, meanL)
        toplefttext(ax, '{}\n{:.0f}% low type\n$\\langle$SWL$_{{L}}\\rangle$={:.1f}\n$\\langle$SWL$_{{H}}\\rangle$={:.1f}'.format(textbox_title, 100*fracL, meanL, meanH).strip(), frameon=frameon)
    return hb,hb2

def fetch_first_values_to_get_full_support(df, needvalues, swbcol='SWL'):
    """ Given a dataframe df, and a list of values needvalues, return a list of the first rows of df which have each value in needvalues.
    This is useful for ensuring sample with full support, when I have some preference (e.g. most recent year) for how to find missing values
    """
    if not needvalues: return pd.DataFrame()
    assert len(needvalues) == len(set(needvalues)), "needvalues must be unique"
    assert set(needvalues).issubset(set(df[swbcol])), "needvalues must be a subset of the SWL values in df"
    return df[df[swbcol].isin(needvalues)].groupby(swbcol).first().reset_index()

def get_top_N_samples_with_full_support(df, N, swbScale=None, swbcol='SWL'):#dvar, random_state=0, stratify_by=None):
    """ Pass this a shuffled DataFrame, possibly re-ordered by a preference variable, for instance year if you want to get the most recent observations.
    """
    nScale = df[swbcol].nunique()
    if not df[swbcol].max() - df[swbcol].min() == nScale-1:
        raise ValueError(f"swb/core.py/get_top_N_samples_with_full_support: Observed values in this sample (nScale={nScale})are not contiguous")
    if swbScale is not None:
        assert swbScale == nScale
    # Require this to be a known scale??
    scaleinfo = common_swb_scales(nScale)
    needvalues = scaleinfo['centers']
    if not set(needvalues).issubset(set(df[swbcol])):
        return pd.DataFrame # Failure. Otherwise, we can succeed
    N = min(N, len(df))
    # First, hope we get them all in the first go:
    df1,df2 = df[:N], df[N:]
    missingVals = set(needvalues) - set(df1[swbcol])
    if missingVals:
        df1 = df1[:-len(missingVals)]
    outdf = pd.concat([ df1,
                        fetch_first_values_to_get_full_support(df2, missingVals, swbcol=swbcol)])
    return outdf
    
    

def sample_with_full_support_2(df, N, dvar, random_state=0, stratify_by=None):
    """ rewriting using the tools above..."""

def sample_with_full_support(df, N, dvar, random_state=0, stratify_by=None):
    """ Sample N rows without replacement from df, but ensure that each value of dvar exists at least once in the result
    This is useful for testing (is using smaller sample sizes) ordinal models which require full support.
    Makes sure not to *increase* the size of the sample!

    This does shuffle the result, but it should be reproducible by default (random_state=0).

    Behaviour under stratification TBD.
        Proposal: 
            - split N up by number S of strata
            - include all of any stratum with count <= N/S
            - repeat with remaining strata, allocating remaining N
            - until only one stratum left
            - call this method recursively for each stratum

    However, I don't really need full support for each stratum, so I think the above is to be done at the end.

    Rewrite this to make use of fetch_first_values_to_get_full_support(). TO DO
    
    """
    assert stratify_by is None
    nu = len(df[dvar].unique())  # Number of responses in the scale (e.g. 10 or 11)
    assert pd.notnull(df[dvar]).all()  # Caller already dropped null responses
    if N in [None, np.inf, 'inf', 0] or N>len(df): N=len(df)
    df1 = df if N==len(df) else df.sample(N, random_state=random_state)
    if len(df1[dvar].unique()) == nu:
        return df1
    assert nu<N
    assert N<len(df) # Full dataset should surely span
    fullsupport = df.groupby(dvar).first().reset_index()
    assert len(fullsupport) == nu
    print('  Fiddling with first {} rows of data to ensure full support for {}'.format(nu, dvar))
    df1 = pd.concat([ fullsupport,  df1.iloc[nu:]])
    assert len(df1)==N
    assert pd.notnull(df1[dvar]).all()
    assert len(df1[dvar].unique()) == nu
    return df1


def histogram_with_full_support(y, all_swb_values, colname=None, normalize=False):
    """return a Series of value_count()s which includes zeros for missing values of the range."""
    # Needs weights option! TO DO
    outS = pd.Categorical(y, categories=all_swb_values).value_counts()
    if normalize: outS = outS/sum(outS)
    if hasattr(y, 'name'): outS.name = y.name
    if colname: outS.name=colname
    return outS
    
    
def mean_SWB_data_with_offset_correction(df, swbvar, weightvar='weight',
                                         nPoint=None,
                                         fillna=0,
                                         #min_N = np.inf, # Fail if too few samples for a mean?
                                         fail_mode = 'nan', # 'raise' or 'NaN'
):
    """
    return mean and s.e. and pval for a vector of SWB values, making sure they haven't been shifted down to 0-base from 1-base

    For groupby, you likely want something like:

       dfc.groupby('year').apply(lambda adf: pd.Series(dict(zip(['mean','se','pval'],mean_SWB_data_with_offset_correction(adf, 'lifeToday', weightvar='weight')))))


    If you specify nPoint, ie the kind of scale, you can also specify fillna=True. In this case, unobserved values will be allowed. Otherwise with nPoint specified, an error would be perceived if the distribution does have full nPoint support.

    """
    from cpblUtilities.pandas_utils import weightedMeanSE_pandas
    df = df.copy()
    if fillna is True:
        fillna=0
    if weightvar not in df:
        print('  Warning: Weight {} missing from data in mean_SWB_data_with_offset_correction'.format(weightvar))
        df[weightvar]=1
    dfm= weightedMeanSE_pandas(df,[swbvar] ,weightVar=weightvar, as_df=True)
    m,x,mu,se,pval = dfm[['min','max','mean','se_mean','p']].values[0]

    if nPoint is None:
        nPoint = x-m+1
    try:
        scaleD = common_swb_scales(nPoint)
        if not x-m+1 == nPoint:
            if fillna ==0:
                iiiiiiiii
            fooooooooo
            raise ValueError('swb/core.py/common_swb_scales: x-m+1 != nPoint')
    except ValueError as e:
        if fail_mode in ['nan', 'NaN']:
            return np.NaN, np.NaN, np.NaN
        else:
            raise(e)

    
    assert (m,x) in [(0,6), (1,7), (0,10), (0,9), (1,10)]
    if (m,x) in [(0,6), (0,9)]:
        return mu+1, se, pval
    if (m,x) in [ (1,7), (0,10), (1,10)]:
        return mu, se, pval
    raise ValueError('Not sure how to handle this range of SWB values: {}'.format(m,x))


KNOWN_SWB_SCALES = {
    '1-7': dict(
        nPoint=7,
        edges=np.arange(.5,8,1),
        centres= range(1,8),
        fv=[1,4,7]
        ),
    '1-10': dict(
        nPoint=10,
        edges=np.arange(.5,11,1),
        centres= range(1,11),
        fv=[1,5,10],
        ),
    '0-10': dict(
        nPoint=11,
        edges=np.arange(-.5,11,1),
        centres= range(0,11),
        fv=[0,5,10],
        )
    }
for k,v in KNOWN_SWB_SCALES.items():
    v['centers'] = list(v['centres'])

# Did you know:  | is a merge operator for dicts !
KNOWN_SWB_SCALES_BY_NPOINT = dict([(v['nPoint'], v | {'name':k}) for k,v in KNOWN_SWB_SCALES.items()])

def common_swb_scales(nPoint):
    """ utility function for 10-point, 11-point, 7-point scales
returns the integer response options, and the edges for a histogram, and the focal values

nPoint could also be a list of the values, or data....? not written yet
"""
    if nPoint in KNOWN_SWB_SCALES_BY_NPOINT:
        return KNOWN_SWB_SCALES_BY_NPOINT[nPoint]
    """
    if nPoint==7:
        edges=np.arange(.5,8,1)
        centres= range(1,8)
        fv=[1,4,7]
    elif nPoint==10:
        edges=np.arange(.5,11,1)
        centres= range(1,11)
        fv=[1,5,10]
    elif nPoint==11:
        edges=np.arange(-.5,11,1)
        centres= range(0,11)
        fv=[0,5,10]
    else:
        raise ValueError("swb/core.py/common_swb_scales: I don't yet know about SWL with {} response options".format(nPoint))
    return dict(centers=list(centres), centres=centres, fv=fv, edges=edges)

    """
    raise ValueError("swb/core.py/common_swb_scales: I don't yet know about SWL with {} response options".format(nPoint))




    

















def plot_SWL_distributions_by_category(df, swbvar='SWL', categoryvar='education4',  weightvar='weight', categorylabels=None,
                                       ncols=2, axs=None,   frameon=True , focalvalues=[0,5,10], tags=[],
                                       categoriesOrder=None, 
                                       swb_longname=None, groupvar=None, show_fractions=False,
                                       fraction_text_size=6, # This shouldn't need to be variable, but 6 seems too small in one case... Fix to higher?
                                       sharey=False, # I prefer to have each subplot take up its full vertical value. If you prefer not, set to True.
                              ):
    """
Name the swbvar column as you would like it shown in the equation showing the mean.
Specify, if you like, a longer name as swb_longname

if you use groupvar, you should set ncols to be the number of categoryvar values, for best results.
groupvar may not be implemented yet

Sample format:
        categorylabels = {
                    1:'$<$ high school',
                    2:'Graduated high school',
                    3:'Some post-secondary',
                    4:'Post-secondary'}        

show_fractions can be "focal", True, False. Defaults to False. This is to write percentages above each bar.
    """
    from cpblUtilities.mathgraph import human_format
    from cpblUtilities import toplefttext
    if groupvar is not None:
        #print(df.groupby('wave').education3.describe())
        df = df.dropna(subset=[categoryvar,groupvar])
        
    for v in [swbvar,]:
        df[v] = df[v].astype(int)
    categoryvars = [categoryvar,groupvar] if groupvar is not None else [categoryvar]
    
    allSWLvalues = sorted(df.reset_index()[swbvar].unique())
    dfraw=df.copy()
    if not (df.groupby(categoryvars)[swbvar].unique().map(len)==len(allSWLvalues)).all():
        from cpblUtilities.pandas_utils import rectangularize_dataframe
        dfg= df.groupby([swbvar]+categoryvars)[weightvar].sum().reset_index()
        df= rectangularize_dataframe(dfg, categoryvars+[swbvar]).fillna(0)

    allcategs =  sorted(df.reset_index()[categoryvar].unique()) if categoriesOrder is None else categoriesOrder
    
    if categorylabels is None: categorylabels=[str(s) for s in allcategs]
    if not isinstance(categorylabels,dict):
        categorylabels=dict(zip(allcategs, categorylabels))
    assert len(allSWLvalues) in [7,10,11]
    nE = len(allcategs)
    nrows= int(np.ceil(nE/ncols))
    if axs is None:
        fig, axs = plt.subplots(nrows, ncols,figsize=(ncols*3,nrows*3))
        plt.setp(axs,'visible',False)
    else:
        plt.sca(axs[0])
        fig= plt.gcf()
    from cpblUtilities.pandas_utils import weightedMeanSE_pandas

    # Check for and remove empty categories:
    check_sums = df.groupby(categoryvar)[weightvar].sum()
    if (check_sums==0).any():
        bads =check_sums[check_sums==0].index
        df = df[~df[categoryvar].isin(   bads  )]
        allcategs = [ae for ae in allcategs if ae not in bads]
        

    means=df.reset_index().groupby(categoryvar).apply(lambda adf: weightedMeanSE_pandas(adf,swbvar,weightvar))
    handles=[]
    counts =  dfraw.groupby(categoryvar)[swbvar].count().to_dict()


    df = df.set_index(categoryvar)
    df['norm'] = df.groupby(level=0).weight.sum()
    df[weightvar] = df[weightvar]/df.norm
    from cpblUtilities.mathgraph import format_to_n_sigfigs
    ymax = -np.inf
    for iax, educ in enumerate(allcategs):
        dist = df.loc[educ].groupby(swbvar)[[weightvar]].sum().reset_index()
        ax =axs.flatten()[iax]
        hh= plot_bar_by_SWL(dist[swbvar].values, dist[weightvar].values, ax=ax, fv=focalvalues)
        ymax = max(ax.get_ylim()[1],ymax)
    for iax, educ in enumerate(allcategs): # Loop again just so we can coordinate y axies (sharey) if needed
        dist = df.loc[educ].groupby(swbvar)[[weightvar]].sum().reset_index()
        ax =axs.flatten()[iax]
        if sharey:
            ax.set_ylim(0,ymax)

        dist['pct'] = 100*dist[weightvar]/ dist[weightvar].sum()
        dist['pct'] = dist['pct'].map(lambda x: format_to_n_sigfigs(x, 2, nanstr='', maxval=None, maxstr='large', minval=None, minstr='0', min_sci_notation=None))
        dist = dist.set_index(swbvar, drop=False)
        for ss in focalvalues if show_fractions == 'focal' else dist[swbvar].values if show_fractions is True else []:
            vx = ax.get_ylim()[1] # Assume y axis starts at 0
            vpOut,vpIn = (  dist.loc[ss,weightvar]+.01*vx   , dist.loc[ss,weightvar]-.01*vx)
            # Put text above bar, unless it's near the top, in which case below bar:
            # vp,va = (vpOut,'bottom') if vpOut<.9*ax.get_ylim()[1] else (vpIn,'top')
            # No, put text inside bar, if we can:
            vp,va,color = (vpOut,'bottom','k') if vpIn<.05*vx else (vpIn,'top','white')
            ax.text(ss,vp , dist.loc[ss,'pct']+'%', va=va, ha='center',size=fraction_text_size, color=color)

        ax.set_visible( True)
        ax.set_yticklabels([])
        plt.savefig('footmp.png')
        ax.set_xlabel('')
        if iax%ncols==0:
            ax.set_ylabel('Weighted relative frequency')

        ht=toplefttext(ax, '\n'.join([categorylabels[educ],
                                      r'$\langle{{\rm {swbvar}}}\rangle={mu:.2f}\pm{se2:.2f}$'.format(swbvar=swbvar, mu=means.loc[educ,swbvar], se2= 1.96* means.loc[educ,'se_'+swbvar]),
                                      'N={}'.format( human_format( counts[educ]))]),
                                      #'N={}'.format( human_format( means.loc[educ,'N_'+swbvar] ))]),
                       10, frameon=frameon)
                                     
                                     
        handles+= [[hh,ht]]
        
    # Instead of fig.supxlabel(swbvar), which is big and ugly (makes space for axis xlabels):
    ax0 = fig.add_subplot(111, frame_on=False)   # creating a single axes
    ax0.set_xticks([])
    ax0.set_yticks([])
    ax0.set_xlabel(swbvar if swb_longname is None else swb_longname , labelpad=25)
    
    fig.tight_layout()         # Recommended
    return fig,axs, list(zip(*handles))


